/***************************************
*************************************************************
*	Program to convert Kilometers into Meters and Centimeters	    *
*************************************************************
Enter the distance in kilometers : 63

The distance entered in kilometers is : 63.000 

Equivalent distance in meters is : 63000.000 

Equivalent distance in centimeters is : 6300000.000 

***************************************/
