/***************************************************************************
*File			: A04BalanceChemEqn.c
*Description	: Program to balance the given Chemical Equation 
					of the form  b1*Ax + b2*By ==> b3(ApBq)
*Author			: Prabodh C P
*Compiler		: gcc compiler, Ubuntu 22.04
*Date			: 26 December 2022
***************************************************************************/
#include<stdio.h>
#include<stdlib.h>

int fnGCD(int, int );

/***************************************************************************
*Function		: 	main
*Input parameters	:	no parameters
*RETURNS		:	0 on success
***************************************************************************/
int main(void)
{
	int x, y, p, q;
	int b1, b2, b3;
	int iCommDivisor;

	printf("Enter the atomocity(x) of Element1 : "); 	scanf("%d", &x);
	printf("Enter the atomocity(y) of Element2 : "); 	scanf("%d", &y);
	printf("Enter the atomocity(p) of Element1 in the compound : "); 	scanf("%d", &p);	
	printf("Enter the atomocity(q) of Element2 in the compound : "); 	scanf("%d", &q);	
		
	b1 = p * y;
	b2 = q * x;
	b3 = x * y;

	//if b1, b2 and b3 together have a greatest common divisor divide each one by that greatest common divisor 
	iCommDivisor = fnGCD(b1,b2);
	iCommDivisor = fnGCD(b3, iCommDivisor);

	b1 = b1 / iCommDivisor;
	b2 = b2 / iCommDivisor;
	b3 = b3 / iCommDivisor;

	printf("\nx = %d\ty = %d\tp = %d\tq = %d\n", x, y, p, q);
	printf("\nb1 = %d\tb2 = %d\tb3 = %d\n", b1, b2,b3);
	printf("\nBalanced Equation is now :\n\t%d*%d + %d*%d ==> %d(%d,%d)\n", b1,x,b2,y,b3,p,q);
	
	return 0;
		
}
/***************************************************************************
*Function		: 	fnGCD
*Description	:	function to calculate GCD of two numbers
*Input parameters	:	iVal1 - non-negative integer
						iVal2 - non-negative integer
*RETURNS		:	greatest common divisor of iVal1 and iVal2
***************************************************************************/
int fnGCD(int iVal1, int iVal2)
{
	if (0 == iVal2)
		return iVal1;
	return fnGCD(iVal2, iVal1 % iVal2);
}

